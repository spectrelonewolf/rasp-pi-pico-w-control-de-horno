import network
import time
from time import sleep
import socket
from machine import Pin,PWM
import micropydatabase as db
import ujson
import utime
import os

################## VARIABLES Y EJECUCIONES INICIALES ###################

#apago el led de la placa madre
led = Pin("LED", Pin.OUT)
led.value(0)

#seteo los valores de los 3 leds conectados en la placa
greenLed = 11
red1Led = 12
red2Led = 13
gLed = PWM(Pin(greenLed))
gLed.freq(1000)
gLed.duty_u16(0)
r1Led = PWM(Pin(red1Led))
r1Led.freq(1000)
r1Led.duty_u16(0)
r2Led = PWM(Pin(red2Led))
r2Led.freq(1000)
r2Led.duty_u16(0)
contador = 0

#Test Leds
gLed.duty_u16(0)
r1Led.duty_u16(0)
r2Led.duty_u16(0)
print("leds apagados ")
sleep(1)
gLed.duty_u16(15000)
r1Led.duty_u16(15000)
r2Led.duty_u16(15000)
print("leds media potencia")
sleep(1)
gLed.duty_u16(65550)
r1Led.duty_u16(65550)
r2Led.duty_u16(65550)
print("leds full potencia")
sleep(1)
gLed.duty_u16(0)
r1Led.duty_u16(0)
r2Led.duty_u16(0)

#seteo los switch o pulsadores
sensorAcerco = 21
sendorEmpujo = 20
pulsadorAcerco = 19
pulsadorEmpujo = 18
senAcer = Pin(sensorAcerco, Pin.IN, Pin.PULL_UP)
senEmp = Pin(sendorEmpujo, Pin.IN, Pin.PULL_UP)
pulAcer = Pin(pulsadorAcerco, Pin.IN, Pin.PULL_UP)
pulEmp = Pin(pulsadorEmpujo, Pin.IN, Pin.PULL_UP)


    
################## ACCESS POINT - PAGINA WEB ###################

#Modo AP, es la funcion principal ya que maneja un loop interno constante (True)
ultima_insercion = 0
def ap_mode(ssid, password):
    ap = network.WLAN(network.AP_IF)
    ap.config(essid=ssid, password=password)
    ap.active(True)
    try:
        while not ap.active():
            pass
        print('AP Mode Is Active, You can Now Connect')
        print('IP Address To Connect to:', ap.ifconfig()[0])

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(('', 80))
        s.listen(5)
        
        ultima_insercion = 0
        
        while True:
            check_datos()
            conn, addr = s.accept()
            print('Got a connection from %s' % str(addr))
            request = conn.recv(1024)
            if request:
                led.value(1)  # Enciende el LED cuando hay una conexión
            else:
                led.value(0)  # Apaga el LED cuando no hay conexión

            request_str = str(request)
            if "/add_data" in request_str:  # Si se presionó el botón "Agregar Dato"
                tiempo_actual = utime.mktime(utime.localtime())
                tiempo_transcurrido = tiempo_actual - ultima_insercion
                if tiempo_transcurrido >= 2:  # Evita inserciones dentro de un intervalo de 10 segundos
                    insert_data(str(time.localtime()))
                    ultima_insercion = tiempo_actual  # Actualiza el tiempo de la última inserción
                response = web_page()
                conn.send(response)
            else:
                response = web_page()
                conn.send(response)
            conn.close()
    except KeyboardInterrupt:
        # Manejo de la interrupción de teclado (Ctrl+C)
       stop_web_server(s, ap)  # Asegúrate de desconectar el servidor antes de salir
       print("Servidor detenido por SWI.") 

#Detiene el servidor, cierra el socket, apaga el wifi.
def stop_web_server(s, ap):
    # Detén el servidor web o desconecta el puerto 80
    print("Cerrando Sockets y Desactivando WIFI")
    s.close()  # Cierra el socket
    ap.active(False)
    # Realiza cualquier otra acción de desconexión necesaria

#Armo mi pagina web con la tabla incluidos los datos de mi base de datos
def web_page():
    data = get_data()
    html = """<html>
    <head><meta name="viewport" content="width=device-width, initial-scale=1"></head>
    <body>
        <h1> Actividad de Horno - Lavagcer </h1>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Value</th>
                <th>Timestamp</th>
            </tr>
    """
    for tupla in data:
        tiempo_local = tupla['timestamp']
        fechaformateada = "{:02}/{:02}/{:04} {:02}:{:02}".format(tiempo_local[2], tiempo_local[1], tiempo_local[0], tiempo_local[3], tiempo_local[4])
        html += "<tr><td>{}</td><td>{}</td><td>{}</td></tr>".format(tupla['_row'], tupla['value'], fechaformateada)
    html += """</table>
    <form action="/add_data" method="post">
        <input type="submit" value="Agregar Dato">
    </form>
    </body>
    </html>"""
    return html

#####################BASE DE DATOS#########################
def check_database_existence(database_name):
    try:
        db.Database.open(database_name)
        return True  # La base de datos existe
    except Exception:
        return False  # La base de datos no existe

# Función para crear la base de datos y tabla (llamada una vez)
def setup_database():
    database_name = "data"
    if not check_database_existence(database_name):
        db_object = db.Database.create(database_name)
        db_table = db_object.create_table("sensor_data", {"value": str, "timestamp": str })

# Funcion para insertar datos en la base de datos
def insert_data(value):
    db_object = db.Database.open("data")
    db_table = db_object.open_table("sensor_data")
    db_table.insert({"value": value, "timestamp": str(time.localtime())})

# OBtengo los datos de la mejor manera que puedo libreria del OGT
def get_data():
    db_object = db.Database.open("data")
    db_table = db_object.open_table("sensor_data")
    all_records = db_table.scan()
    # Itera a través de los registros y haz lo que necesites con ellos
    data = []  # Lista para almacenar los datos reorganizados
    contador = 0
    for record in all_records:
        contador += 1
        row = str(contador)
        value = eval(record['value'])  # Convierte la cadena en una tupla
        timestamp = eval(record['timestamp'])  # Convierte la cadena en una tupla

        # Crea un nuevo diccionario con las claves que necesitas
        nuevo_dato = {'_row': row, 'value': value, 'timestamp': timestamp}

        data.append(nuevo_dato)
    return data

# Verificar y borrar datos si hay más de 5 archivos en la base de datos
def check_datos():
    db_object = db.Database.open("data")
    db_table = db_object.open_table("sensor_data")
    datos = get_data()
    contador = len(datos)
    if contador > 55:
        print("mas de 55 almacenamientos, borrando datos")
        truncate_data_files()

def truncate_data_files():
    # Cambia al directorio donde están los archivos de datos
    os.chdir("data/sensor_data")

    # Lista todos los archivos en el directorio
    files = os.listdir(".")

    tables_list = []

    # Filtra los archivos que comienzan con 'data' y son archivos regulares
    for item in files:
        if item.startswith('data'):
            tables_list.append(item)

    # Ordena los archivos por nombre para que los más antiguos estén al principio
    tables_list.sort(key=lambda x: int(x[4:x.index('_')]))

    # Calcula cuántos archivos necesitas eliminar
    files_to_delete = len(tables_list) - 5

    # Borra los archivos comenzando desde la decena más antigua de datos
    for i in range(files_to_delete):
        os.remove(tables_list[i])
    
    os.chdir('/')
    os.chdir('/')


setup_database()
ap_mode('Lavagcer', '123456789')
