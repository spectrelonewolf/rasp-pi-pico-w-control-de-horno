import network
import time
from time import sleep
import socket
from machine import Pin,PWM
import micropydatabase as db
import ujson
import utime
import os
import _thread

################## VARIABLES Y EJECUCIONES INICIALES ###################

# Apago el LED de la placa
led = Pin("LED", Pin.OUT)
led.value(0)

# Seteo los valores de los 3 LEDs conectados en la placa
greenLed = 11
red1Led = 12
red2Led = 13
gLed = PWM(Pin(greenLed))
gLed.freq(1000)
gLed.duty_u16(0)
r1Led = PWM(Pin(red1Led))
r1Led.freq(1000)
r1Led.duty_u16(0)
r2Led = PWM(Pin(red2Led))
r2Led.freq(1000)
r2Led.duty_u16(0)
contador = 0

# Test Leds
gLed.duty_u16(0)
r1Led.duty_u16(0)
r2Led.duty_u16(0)
print("leds apagados ")
sleep(1)
gLed.duty_u16(15000)
r1Led.duty_u16(15000)
r2Led.duty_u16(15000)
print("leds media potencia")
sleep(1)
gLed.duty_u16(65550)
r1Led.duty_u16(65550)
r2Led.duty_u16(65550)
print("leds full potencia")
sleep(1)
gLed.duty_u16(0)
r1Led.duty_u16(0)
r2Led.duty_u16(0)

# Seteo los switch o pulsadores
sensorAcerco = 21
sendorEmpujo = 20
pulsadorAcerco = 19
pulsadorEmpujo = 18
senAcer = Pin(sensorAcerco, Pin.IN, Pin.PULL_UP)
senEmp = Pin(sendorEmpujo, Pin.IN, Pin.PULL_UP)
pulAcer = Pin(pulsadorAcerco, Pin.IN, Pin.PULL_UP)
pulEmp = Pin(pulsadorEmpujo, Pin.IN, Pin.PULL_UP)

################## SECCIÓN DE PROGRAMA PRINCIPAL #################

# Tiempo de secciones
timer = 0
# Flags, semáforos
horneando = 0
empujando = 0
acercando = 0
# Definición de función principal
def verificarTiempoSensoresYPulsadores():
    print("Iniciando Thread de control de sensores y pulsadores")
    global horneando, empujando, acercando
    global senAcer, senEmp, pulAcer, pulEmp
    # Comienzo de lógica
    while True:
        # Obtener valores de los sensores y pulsadores
        sAState = senAcer.value()
        sEState = senEmp.value()
        pAState = pulAcer.value()
        pEState = pulEmp.value()     
        if horneando == 1:
            gLed.duty_u16(65550)
        else:
            gLed.duty_u16(0)
        if empujando == 1 and transcurrioTiempo(timer, 10): # Pasan 30 (1800 segundos) minutos desde que esta empujando
                r1Led.duty_u16(0)
                r2Led.duty_u16(65550)
                sleep(0.1)
                r2Led.duty_u16(15000)
                sleep(0.1)
                r2Led.duty_u16(0)
                sleep(0.1)
                r2Led.duty_u16(65550)
                sleep(0.1)
                r2Led.duty_u16(15000)
                sleep(0.1)
                r2Led.duty_u16(0)
                sleep(0.1)
        elif acercando == 1 and transcurrioTiempo(timer, 15): # Pasan 35 (2100 segundos) minutos desde que esta acercar
                r2Led.duty_u16(0)
                r1Led.duty_u16(65550)
                sleep(0.1)
                r1Led.duty_u16(15000)
                sleep(0.1)
                r1Led.duty_u16(0)
                sleep(0.1)
                r1Led.duty_u16(65550)
                sleep(0.1)
                r1Led.duty_u16(15000)
                sleep(0.1)
                r1Led.duty_u16(0)
                sleep(0.1)   
        if pAState == 0 or pEState == 0:
            # Comienza la horneada
            horneando = 1
            if pAState == 0 and acercando == 0:
                print(str(sAState) + str(sEState) + str(pAState)+ str(pEState))
                tiempo_actual = utime.mktime(utime.localtime())
                timer = tiempo_actual
                r2Led.duty_u16(65550)
                r1Led.duty_u16(0)
                print("pulsador Acercar")
                insert_data(str('p_acercar'))
                acercando = 1
                empujando = 0
                sleep(1)
            elif pEState == 0 and empujando == 0:
                print(str(sAState) + str(sEState) + str(pAState)+ str(pEState))
                tiempo_actual = utime.mktime(utime.localtime())
                timer = tiempo_actual
                r1Led.duty_u16(65550)
                r2Led.duty_u16(0)
                print("pulsador Empujar")
                insert_data(str('p_empujar'))
                empujando = 1
                acercando = 0
                sleep(1)
        elif sAState == 0 or sEState == 0:
            if sAState == 0 and horneando == 1 and acercando == 0:
                print(str(sAState) + str(sEState) + str(pAState)+ str(pEState))
                tiempo_actual = utime.mktime(utime.localtime())
                timer = tiempo_actual
                r2Led.duty_u16(65550)
                r1Led.duty_u16(0)
                print("sensor Acercar")
                insert_data(str('s_acercar'))
                acercando = 1
                empujando = 0
                sleep(1)
            elif sEState == 0 and horneando == 1 and empujando == 0:
                print(str(sAState) + str(sEState) + str(pAState)+ str(pEState))
                tiempo_actual = utime.mktime(utime.localtime())
                timer = tiempo_actual
                r1Led.duty_u16(65550)
                r2Led.duty_u16(0)
                print("sensor Empujar")
                insert_data(str('s_empujar'))
                empujando = 1
                acercando = 0
                sleep(1)

def transcurrioTiempo(ultima_insercion, lapso):
    tiempo_actual = utime.mktime(utime.localtime())
    tiempo_transcurrido = tiempo_actual - ultima_insercion
    if tiempo_transcurrido >= lapso:  # Evita inserciones dentro de un intervalo de 2 segundos
        ultima_insercion = tiempo_actual
        return True
    else:
        return False

################## ACCESS POINT - PAGINA WEB ###################

# Modo AP, es la función principal ya que maneja un loop interno constante (True)
ultima_insercion = 0
def ap_mode(ssid, password):
    ap = network.WLAN(network.AP_IF)
    ap.config(essid=ssid, password=password)
    ap.active(True)
    try:
        while not ap.active():
            pass
        print('AP Mode Is Active, You can Now Connect')
        print('IP Address To Connect to:', ap.ifconfig()[0])

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(('', 80))
        s.listen(2)
        
        ultima_insercion = 0
        
        while True:
            check_datos()
            current_time = get_current_time()  # Obtiene la hora y fecha actual
            conn, addr = s.accept()
            print('Got a connection from %s' % str(addr))
            request = conn.recv(1024)
            if request:
                led.value(1)  # Enciende el LED cuando hay una conexión
            else:
                led.value(0)  # Apaga el LED cuando no hay conexión
            request_str = str(request)
            if "/update_data" in request_str:  # Si se presionó el botón "Agregar Dato"
                response = web_page()
                conn.send(response)
            elif "/set_time" in request_str:
                response = set_time_page()
                conn.send(response)
            elif "/update_time" in request_str:
                response = update_time(request_str)
                conn.send(response)
            else:
                response = web_page()
                conn.send(response)
            conn.close()
    except KeyboardInterrupt:
        # Manejo de la interrupción de teclado (Ctrl+C)
       stop_web_server(s, ap)  # Asegúrate de desconectar el servidor antes de salir
       print("Servidor detenido por SWI.") 

# Detiene el servidor, cierra el socket, apaga el WiFi
def stop_web_server(s, ap):
    # Detén el servidor web o desconecta el puerto 80
    print("Cerrando Sockets y Desactivando WIFI")
    s.close()  # Cierra el socket
    ap.active(False)
    # Realiza cualquier otra acción de desconexión necesaria

# Armo mi página web con la tabla incluidos los datos de mi base de datos
def web_page():
    data = get_data()
    data.reverse()  # Invertir la lista de datos
    html = """<html>
    <head>
    <meta name="viewport" content="width=device-width, initial-scale=1"></head>
    <link rel="icon" href="data:;base64,iVBORw0KGgo="> <!-- Agrega el enlace al favicon vacío -->
    <body>
        <h1> Actividad de Horno - Lavagcer </h1>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Value</th>
                <th>Timestamp</th>
            </tr>
    """
    for tupla in data:
        tiempo_local = tupla['timestamp']
        fecha_almacenada = "{:02}/{:02}/{:04} {:02}:{:02}".format(tiempo_local[2], tiempo_local[1], tiempo_local[0], tiempo_local[3], tiempo_local[4])
        
        # Obtiene la hora y fecha reales del dispositivo
        fecha_real = get_current_time()
        
        html += "<tr><td>{}</td><td>{}</td><td>Real: {} - Almacenado: {}</td></tr>".format(tupla['_row'], tupla['value'], fecha_real, fecha_almacenada)
    
    html += """</table>
    <form action="/update_data" method="post">
        <input type="submit" value="Actualizar Página">
    </form>
    
    <form action="/set_time" method="get">
        <input type="submit" value="Establecer Hora y Fecha">
    </form>
    
    <h2>Hora y Fecha Actual:</h2>
    <p>{}</p>  <!-- Esto mostrará la hora y la fecha actual -->
    
    </body>
    </html>""".format(get_current_time())
    
    return html

# Formulario para establecer la hora y fecha reales
def set_time_page():
    html = """<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="data:;base64,iVBORw0KGgo="> <!-- Agrega el enlace al favicon vacío -->
    </head>
    <body>
        <h1> Establecer Fecha y Hora Reales </h1>
        <form action="/update_time" method="post">
            <input type="text" name="real_time" placeholder="YYYY-MM-DD HH:MM">
            <input type="submit" value="Actualizar Fecha y Hora">
        </form>
        <h2>Hora y Fecha Actual:</h2>
    <p>{}</p>  <!-- Esto mostrará la hora y la fecha actual -->
    
    </body>
    </html>""".format(get_current_time())
    return html

# Actualizar la hora y fecha reales
def update_time(request):
    try:
        if request:
            led.value(1)  # Enciende el LED cuando hay una conexión
            request_str = str(request)
            print("Request String:", request_str)  # Debug: Imprimir la solicitud
            if "/update_time" in request_str:
                time_start = request_str.index("real_time=") + len("real_time=")
                time_end = request_str.index("'", time_start)
                if time_end == -1:
                    time_end = len(request_str)
                real_time_encoded = request_str[time_start:time_end]
                real_time = decode_url(real_time_encoded)  # Decodificar la cadena
                print("Real Time String (Decoded):", real_time)  # Debug: Imprimir el valor de "real_time"
                try:
                    # Dividir la cadena en fecha y hora
                    date_str, time_str = real_time.split(' ')
                    year, month, day = map(int, date_str.split('-'))
                    hour, minute = map(int, time_str.split(':'))

                    # Establecer la fecha y la hora
                    rtc = machine.RTC()
                    rtc.datetime((year, month, day, 0, hour, minute, 0, 0))
                    print("Fecha y hora actualizadas correctamente")
                except Exception as e:
                    print("Error al actualizar la fecha y hora:", str(e))
            response = web_page()
            return response
        else:
            led.value(0)  # Apaga el LED cuando no hay conexión
            return web_page()
    except Exception as e:
        print("Error general:", str(e))  # Debug: Imprimir cualquier error general
        return web_page()


# Función para decodificar la cadena codificada en URL
def decode_url(url):
    decoded_url = url.replace("+", " ").replace("%3A", ":")
    return decoded_url
    
# Actulizar fecha y hora
def get_current_time():
    current_time = utime.localtime()
    formatted_time = "{:02}/{:02}/{:04} {:02}:{:02}".format(current_time[2], current_time[1], current_time[0], current_time[3], current_time[4])
    return formatted_time

#####################BASE DE DATOS#########################
def check_database_existence(database_name):
    try:
        db.Database.open(database_name)
        return True  # La base de datos existe
    except Exception:
        return False  # La base de datos no existe

# Función para crear la base de datos y tabla (llamada una vez)
def setup_database():
    database_name = "data"
    if not check_database_existence(database_name):
        db_object = db.Database.create(database_name)
        db_table = db_object.create_table("sensor_data", {"value": str, "timestamp": str })

# Funcion para insertar datos en la base de datos
def insert_data(value):
    db_object = db.Database.open("data")
    db_table = db_object.open_table("sensor_data")
    db_table.insert({"value": value, "timestamp": str(time.localtime())})

# Obtengo los datos de la mejor manera que puedo libreria del OGT
def get_data():
    db_object = db.Database.open("data")
    db_table = db_object.open_table("sensor_data")
    all_records = db_table.scan()
    # Itera a través de los registros y haz lo que necesites con ellos
    data = []  # Lista para almacenar los datos reorganizados
    contador = 0
    for record in all_records:
        contador += 1
        row = str(contador)
        value = str(record['value'])  # Convierte la cadena en una tupla
        timestamp = eval(record['timestamp'])  # Convierte la cadena en una tupla
        # Crea un nuevo diccionario con las claves que necesitas
        nuevo_dato = {'_row': row, 'value': value, 'timestamp': timestamp}
        data.append(nuevo_dato)
    return data

# Verificar y borrar datos si hay más de 5 archivos en la base de datos
def check_datos():
    db_object = db.Database.open("data")
    db_table = db_object.open_table("sensor_data")
    datos = get_data()
    contador = len(datos)
    if contador > 65:
        print("Mas de 65 almacenamientos, borrando datos")
        truncate_data_files()

def truncate_data_files():
    # Cambia al directorio donde están los archivos de datos
    os.chdir("data/sensor_data")
    # Lista todos los archivos en el directorio
    files = os.listdir(".")
    tables_list = []
    # Filtra los archivos que comienzan con 'data' y son archivos regulares
    for item in files:
        if item.startswith('data'):
            tables_list.append(item)
    # Ordena los archivos por nombre para que los más antiguos estén al principio
    tables_list.sort(key=lambda x: int(x[4:x.index('_')]))
    # Calcula cuántos archivos necesitas eliminar
    files_to_delete = len(tables_list) - 6
    # Borra los archivos comenzando desde la decena más antigua de datos
    for i in range(files_to_delete):
        os.remove(tables_list[i])
    os.chdir('/')
    os.chdir('/')

_thread.start_new_thread(verificarTiempoSensoresYPulsadores, ())
setup_database()
ap_mode('Lavagcer', '123456789')
