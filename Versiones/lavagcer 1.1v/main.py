import network
import time
import socket
from machine import Pin
import micropydatabase as db
import ujson
import utime

led = Pin("LED", Pin.OUT)
led.value(0)
def check_database_existence(database_name):
    try:
        db.Database.open(database_name)
        return True  # La base de datos existe
    except Exception:
        return False  # La base de datos no existe

# Función para crear la base de datos y tabla (llamada una vez)
def setup_database():
    database_name = "data"
    if not check_database_existence(database_name):
        db_object = db.Database.create(database_name)
        db_table = db_object.create_table("sensor_data", {"value": str, "timestamp": str})

def insert_data(value):
    db_object = db.Database.open("data")
    db_table = db_object.open_table("sensor_data")
    db_table.insert({"value": value, "timestamp": str(time.localtime())})

def get_data():
    db_object = db.Database.open("data")
    db_table = db_object.open_table("sensor_data")
    all_records = db_table.scan()
    # Itera a través de los registros y haz lo que necesites con ellos
    data = []  # Lista para almacenar los datos reorganizados
    contador = 0
    for record in all_records:
        contador += 1
        row = str(contador)
        value = eval(record['value'])  # Convierte la cadena en una tupla
        timestamp = eval(record['timestamp'])  # Convierte la cadena en una tupla

        # Crea un nuevo diccionario con las claves que necesitas
        nuevo_dato = {'_row': row, 'value': value, 'timestamp': timestamp}

        data.append(nuevo_dato)
    return data

def web_page():
    print("algo esta pasando batman0")
    data = get_data()
    html = """<html>
    <head><meta name="viewport" content="width=device-width, initial-scale=1"></head>
    <body>
        <h1>Hello World</h1>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Value</th>
                <th>Timestamp</th>
            </tr>
    """
    for tupla in data:
        tiempo_local = tupla['timestamp']
        fechaformateada = "{:02}/{:02}/{:04} {:02}:{:02}".format(tiempo_local[2], tiempo_local[1], tiempo_local[0], tiempo_local[3], tiempo_local[4])
        html += "<tr><td>{}</td><td>{}</td><td>{}</td></tr>".format(tupla['_row'], tupla['value'], fechaformateada)
    print("algo esta pasando batman3")
    html += """</table>
    <form action="/add_data" method="post">
        <input type="submit" value="Agregar Dato">
    </form>
    </body>
    </html>"""
    return html

ultima_insercion = 0
def ap_mode(ssid, password):
    ap = network.WLAN(network.AP_IF)
    ap.config(essid=ssid, password=password)
    ap.active(True)

    while not ap.active():
        pass
    print('AP Mode Is Active, You can Now Connect')
    print('IP Address To Connect to:', ap.ifconfig()[0])

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', 80))
    s.listen(5)
    
    ultima_insercion = 0
    try:
        while True:
            conn, addr = s.accept()
            print('Got a connection from %s' % str(addr))
            request = conn.recv(1024)
            if request:
                led.value(1)  # Enciende el LED cuando hay una conexión
            else:
                led.value(0)  # Apaga el LED cuando no hay conexión

            request_str = str(request)
            if "/add_data" in request_str:  # Si se presionó el botón "Agregar Dato"
                tiempo_actual = utime.mktime(utime.localtime())
                tiempo_transcurrido = tiempo_actual - ultima_insercion
                if tiempo_transcurrido >= 10:  # Evita inserciones dentro de un intervalo de 10 segundos
                    insert_data(str(time.localtime()))
                    ultima_insercion = tiempo_actual  # Actualiza el tiempo de la última inserción
                response = web_page()
                conn.send(response)
            else:
                response = web_page()
                conn.send(response)
            conn.close()
    except KeyboardInterrupt:
        # Manejo de la interrupción de teclado (Ctrl+C)
       stop_web_server()  # Asegúrate de desconectar el servidor antes de salir
       print("Servidor detenido.") 
        
def stop_web_server():
    # Detén el servidor web o desconecta el puerto 80
    s.close()  # Cierra el socket
    # Realiza cualquier otra acción de desconexión necesaria


# Verificar y borrar datos si hay más de 30 registros en la base de datos
def check_and_cleanup_data():
    db_object = db.Database.open("data")
    db_table = db_object.open_table("sensor_data")
    count = len(db_table.scan())
    if count > 30:
        rows = db_table.scan(order_by='_row')
        to_delete = rows[:count - 30]
        for row in to_delete:
            db_table.delete_row(row['_row'])

setup_database()
ap_mode('Lavagcer', '123456789')
