Resetting Flash memory
Pico’s BOOTSEL mode lives in read-only memory inside the RP2040 chip, and can’t be overwritten accidentally. No matter what, if you hold down the BOOTSEL button when you plug in your Pico, it will appear as a drive onto which you can drag a new UF2 file. There is no way to brick the board through software. However, there are some circumstances where you might want to make sure your Flash memory is empty. You can do this by dragging and dropping a special UF2 binary onto your Pico when it is in mass storage mode.

Download the UF2 file https://datasheets.raspberrypi.com/soft/flash_nuke.uf2?_gl=1*nvnpg4*_ga*ODExNTY0NjU5LjE3MjkyNzYyMTI.*_ga_22FD70LWDS*MTcyOTI3NjIxMS4xLjEuMTcyOTI3Njg0MS4wLjAuMA..

See the code on Github