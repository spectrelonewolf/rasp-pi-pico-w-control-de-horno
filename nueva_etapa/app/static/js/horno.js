// CONSTANTES
DIR_IMG = "img/";
LARGO = "1180";
ALTO = "480";
POSX_INICIAL = Math.round(LARGO / 2) - 15;
POSY_INICIAL = ALTO - 40;
POSX_ESCENARIO = 0;
POSY_ESCENARIO = 0;
Programa_FPS = 50;
var micanvas;
var contexto;
var posiciones_horno = [
  [1025, 65],
  [980, 65],
  [935, 65],
  [890, 65],
  [845, 65],
  [800, 65],
  [755, 65],
  [710, 65],
  [665, 65],
  [620, 65],
  [575, 65],
  [530, 65],
  [485, 65],
  [440, 65],
  [395, 65],
  [350, 65],
  [305, 65],
  [260, 65],
  [215, 65],
  [170, 65],
  [140, 200],
  [185, 200],
  [935, 200],
  [980, 200],
  [1070, 110],
  [1070, 65],
];

var posiciones_zorras_reinicio = [6, 5, 4, 3, 2, 1];

var posiciones_zorras = new Array(26).fill(null);

///////////////////////////////////////////////////////////////////
//                                      Inicio
///////////////////////////////////////////////////////////////////

// quokka-ignore
window.onload = function () {
  micanvas = document.getElementById("espacio");
  contexto = micanvas.getContext("2d");
  // Hacer la solicitud al backend para obtener las posiciones de las zorras
  fetchZorraData();
  //var programa = new Programa();
};

///////////////////////////////////////////////////////////////////
//                              Fetch data from backend
///////////////////////////////////////////////////////////////////
function fetchZorraData() {
    fetch("/iniciar_horno", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        // Verificar si el horno debe reiniciarse o continuar
        if (data.mensaje.includes("Reinicio")) {
          // Si es un reinicio, limpiar o reiniciar la vista
          reiniciarVista();
        } else {
          // Si se continúa el ciclo, simplemente actualizar las posiciones
          let posicionesZorras = data.posiciones_zorras;
          iniciarPrograma(posicionesZorras);
        }
      })
      .catch((error) => {
        console.error("Error fetching zorra data:", error);
      });
  }
  
  function reiniciarVista() {
    // Aquí puedes definir lo que ocurre al reiniciar la vista
    console.log("Reiniciando la vista del horno...");
    // Reiniciar el estado del frontend o actualizar el canvas
  }

///////////////////////////////////////////////////////////////////
//                                      Eventos
///////////////////////////////////////////////////////////////////

// Función para hacer la solicitud POST
function enviarAccion(accion) {
  // Crear los datos a enviar
  const data = { accion: accion };

  // Hacer la solicitud POST usando fetch
  fetch("/proceso_horno", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  })
    .then((response) => response.json())
    .then((result) => {
      // Mostrar el resultado de la respuesta
      alert(result.mensaje || result.error);
    })
    .catch((error) => {
      console.error("Error en la solicitud:", error);
    });
}

// Funciones asociadas a los botones
function acercamiento() {
  enviarAccion("acercamiento");
}

function empuje() {
  enviarAccion("empuje");
}

var ManejadorDeEventos = function (Zorra) {
  this.Zorra = Zorra;

  this.tecla = function (e) {
    // se obtiene el evento
    var evento = e || window.event;

    switch (evento.keyCode) {
      case 97:
        Zorra.moverIzquierda();
        break;

      case 100:
        Zorra.moverDerecha();
        break;

      case 115:
        Zorra.moverAbajo();
        break;

      case 119:
        Zorra.moverArriba();
        break;

      case 103:
        Zorra.moverArriba();
        break;
    }

    return 0;
  };

  document.body.onkeypress = this.tecla;
};

///////////////////////////////////////////////////////////////////
//                                       Escenario
///////////////////////////////////////////////////////////////////

var Escenario_horno = function () {
  //atributos
  this.posx = new Number(POSX_ESCENARIO);
  this.posy = new Number(POSY_ESCENARIO);
  this.figura = new Image();
  this.figura.src = DIR_IMG + "edicion_escenario_horno.png";

  this.dibujar = function () {
    var figura = this.getFigura();
    var x = this.getX();
    var y = this.getY();
    contexto.drawImage(figura, x, y, 1180, 480);
  };

  this.getX = function () {
    return this.posx;
  };

  this.getY = function () {
    return this.posy;
  };

  this.getFigura = function () {
    return this.figura;
  };
};

///////////////////////////////////////////////////////////////////
//                                       Zorra
///////////////////////////////////////////////////////////////////
class Zorra {
  constructor(
    nombre = "",
    posx = POSX_INICIAL,
    posy = POSY_INICIAL,
    estado = "pendiente"
  ) {
    // Atributos
    this.posx = posx;
    this.posy = posy;
    this.figura = new Image();
    this.figura.src = DIR_IMG + "zorra.png";

    // Nuevos atributos
    this.nombre = nombre; // Nombre o número de la zorra
    this.horaAcercamiento = null; // Hora en formato "YYYYMMDDHHMMSS"
    this.horaEmpuje = null;
    this.horaCoccionInicio = null;
    this.horaCoccionFin = null;
    this.estado = estado; // Estados posibles: pendiente, acercada, empujada, cocinada
  }

  // Método para dibujar la zorra
  dibujar() {
    let figura = this.getFigura();
    let x = this.getX();
    let y = this.getY();

    if (isNaN(x) || isNaN(y)) {
      x = Math.round(LARGO / 2);
      y = ALTO + 15;
    }

    contexto.drawImage(figura, x, y, 42, 53);
    let textoZorra = this.nombre;
    dibujarTexto(textoZorra, x + 15, y + 15, 20);
    console.log(textoZorra);
  }

  // Setters
  setX(px) {
    this.posx = px;
  }

  setY(py) {
    this.posy = py;
  }

  setNombre(nombre) {
    this.nombre = nombre;
  }

  setHoraAcercamiento(hora) {
    this.horaAcercamiento = hora;
  }

  setHoraEmpuje(hora) {
    this.horaEmpuje = hora;
  }

  setHoraCoccionInicio(hora) {
    this.horaCoccionInicio = hora;
  }

  setHoraCoccionFin(hora) {
    this.horaCoccionFin = hora;
  }

  setEstado(estado) {
    this.estado = estado;
  }

  // Getters
  getX() {
    return this.posx;
  }

  getY() {
    return this.posy;
  }

  getFigura() {
    return this.figura;
  }

  getNombre() {
    return this.nombre;
  }

  getHoraAcercamiento() {
    return this.horaAcercamiento;
  }

  getHoraEmpuje() {
    return this.horaEmpuje;
  }

  getHoraCoccionInicio() {
    return this.horaCoccionInicio;
  }

  getHoraCoccionFin() {
    return this.horaCoccionFin;
  }

  getEstado() {
    return this.estado;
  }

  // Métodos para mover la zorra
  moverIzquierda() {
    this.setX(this.getX() - 10); // Mueve 10 px a la izquierda
  }

  moverDerecha() {
    this.setX(this.getX() + 10); // Mueve 10 px a la derecha
  }

  moverArriba() {
    this.setY(this.getY() - 10); // Mueve 10 px hacia arriba
  }

  moverAbajo() {
    this.setY(this.getY() + 10); // Mueve 10 px hacia abajo
  }

  toString() {
    return `Zorra ${this.nombre} - Posición: (${this.posx}, ${this.posy}) - Estado: ${this.estado} - Hora Acercamiento: ${this.horaAcercamiento} - Hora Empuje: ${this.horaEmpuje} - Hora Cocción Inicio: ${this.horaCoccionInicio} - Hora Cocción Fin: ${this.horaCoccionFin}`;
  }
}

///////////////////////////////////////////////////////////////////
//                             Circuito Zorras
///////////////////////////////////////////////////////////////////
/*
pos 1 = 1025 - 65
pos 2 = 980 - 65
pos 3 = 935 - 65
pos 4 = 890 - 65
pos 5 = 845 - 65
pos 6 = 800 - 65
pos 7 = 755 - 65
pos 8 = 710 - 65
pos 9 = 665 - 65
pos 10 = 620 - 65
pos 11 = 575 - 65
pos 12 = 530 - 65
pos 13 = 485 - 65
pos 14 = 440 - 65
pos 15 = 395 - 65
pos 16 = 350 - 65
pos 17 = 305 - 65
pos 18 = 260 - 65
pos 19 = 215 - 65
pos 20 = 170 - 65
pos 21 = 140 - 200
pos 22 = 185 - 200
pos 23 = 935 - 200
pos 24 = 980 - 200
pos 25 = 1070 - 110 acercar
pos 26 = 1070 - 65 empujar
*/

///////////////////////////////////////////////////////////////////
//                              Programa con posiciones zorras
///////////////////////////////////////////////////////////////////
function iniciarPrograma(posicionesZorras) {
  var fondo = new Escenario_horno();

  // Cargar posiciones iniciales de las zorras en base a lo que llegó del backend
  for (let i = 0; i < posicionesZorras.length; i++) {
    if (posicionesZorras[i] != null) {
      var zorra = new Zorra();
      let posicion = posicionesZorras[i].posicion;
      let posX = posiciones_horno[posicion][0];
      let posY = posiciones_horno[posicion][1];
      zorra.setX(posX);
      zorra.setY(posY);
      zorra.setNombre(posicionesZorras[i].nombre); // Asignar nombre a la zorra si es necesario
      posiciones_zorras[i] = zorra; // Guarda la zorra en el arreglo
    }
  }

  var manejadorZorra = new ManejadorDeEventos(posiciones_zorras[0]); // Pasa la primera zorra al manejador

  this.correr = function () {
    limpiar();
    fondo.dibujar();
    for (let i = 0; i < posiciones_zorras.length; i++) {
      if (posiciones_zorras[i] != null) {
        posiciones_zorras[i].dibujar(); // Dibuja cada zorra
        let posX = posiciones_horno[i][0];
        let posY = posiciones_horno[i][1];
      }
    }
  };

  var intervalId = setInterval(this.correr, 1000 / Programa_FPS);
}

///////////////////////////////////////////////////////////////////
//                             Tfunciones auxiliares
///////////////////////////////////////////////////////////////////

dibujarTexto = function (texto, x, y, tamanio) {
  contexto.fillStyle = "black"; // color del texto
  fontTexto = tamanio + "px" + " Arial";
  contexto.font = fontTexto; // fuente y tamaño del texto
  contexto.textAlign = "left"; // alineación del texto
  contexto.textBaseline = "top"; // baseline del texto
  contexto.fillText(texto, x, y); // dibuja el texto en la pantalla
};

function limpiar() {
  micanvas.width = micanvas.width;
}

