'''/*

* Secuencia de inicio y funcionamiento en gral del horno:
	-Se cargan 5 a 6 zorras dentro del horno, hasta una posicion anterior de la zona
	de coccion.
	-Llegado al punto de coccion de 1200 grados, el hornero puede ya empujar la primer
	zorra con el empujador hidraulico, este proceso de empuje dura 30 minutos.
	-Pasados los 30 minutos de empuje, el hornero debe acercar una zorra a la entrada.
	-Pasados 30 minutos luego del acercamiento de la zorra debe empujar con el empujador hidraulico.
	-Los procesos de acercamiento y empuje se repiten hasta que se termine el ciclo de
	hornedo de piezas, se caiga el horno, es decir, que alguna zorra se bloquee o algun
	otro imprevisto por otro lado tenemos el corte del suministro de luz.

* Interacciones disponibles mediante raspberry pico w;
	Se propone una conexion wifi con el servidor, enviando peticiones web e interactuando
	con el servidor, el cual manejara la parte logica;
	-dos finales de carrera, uno en la zona de acercamiento y el otro en la de empuje
	-dos pulsadores, uno para dar inicio/reinicio/aceptar de horno y el otro para declarar
	finalizacion/caida/cancelar horno.



* Logica Backend:
	-Al momento de iniciar el servidor/back-end se debe:
		-Buscar en la BD si hay zorras almacenadas.
		-Si es que hay cargar las posiciones 0 - 5 en "posiciones_zorras", si no hay zorras
		cargar "posiciones_zorras_reinicio" en donde se indica en index la posicion y el valor
		el numero/nombre zorra.
	-
	
	-Secuencia de carga del sistema y comienzo del mismo;
		-Comprobar almacenamiento:
			-Verificar ultima zorra ya sea en proceso de acercamiento o empuje:
				Con esto podemos prevenir la secuencia de reinicio total del horno
				en caso del corte de luz o caida de horno, se debe pensar un umbral
				de tiempo para que se considere continuar o reiniciar la secuencia.
			
		-Se superaro el umbral de tiempo de la ultima zorra:
				Este caso es cuando se termino la coccion de horno y quedaron zorras con
				pienzas que no llegaron a su coccion, pero estan lista para coccion desde
				la semana anterior o la ultima coccion en horno.
				Se debe colocar el sistema en espera, esperando el inicio mediante
				el boton de inicio/reinicio/aceptar, ya que no hay aun un control de temperatura
				conectado al servidor, entonces cuando el hornero vea que se llego a los 1200 grados
				presionara el boton para inciar el sistema de avisos, en caso de no presionar y recibir
				mensajes de los finales de carrera se deberia dar aviso sonoro en la terminal.
			-Dilema(a consular con el hornero):
				En el reinicio del horno, se cargan 5-6 zorras para dar inicio a la secuencia
				de acercamiento y empuje, el dilema es: ¿se colocan zorras con informacion cero,
				para luego insertar las horas de entrada y salida de la zona de coccion o se 
				dejan las zorras que se encuentran en memoria en esa zona?, se calcula que
				la ultima opcion, pero se debe confirmar esa informacion, ya que quedaria
				registrado que la zorra ingreso con sus foto e informacion y hay que depurar
				informacion vieja inservible.

	-Se requiere guardar todo tipo de informacion relevante de las zorras:
		-Fecha y hora - formato 20240830052020
		-Acercamiento, en lo posible tomar captura de la zorra con sus piezas a cocinar.
		-Empuje, tanto acercamiento como empuje son mediciones de tiempo.
		-Hora de entrada a zona de coccion
		-Hora de salida de zona de coccion
*/
'''

################################################################IMPORTS#########################################
import os
import time
from flask import Flask, send_from_directory, request, jsonify, session, render_template, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from sqlalchemy import desc, func
from datetime import datetime, timedelta

#################################################################################################################################
# Definicion de la carpeta static para carga de la pagina reutilizada
app = Flask(__name__, static_folder='static', static_url_path='')

# Coonfiguracion de la base de datos dentro de Flask
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///horno.db'
app.config['SECRET_KEY'] = '12344321'
db = SQLAlchemy(app)
migrate = Migrate(app, db)

#################################################################################################################################
# Definicion de los modelos de la base de datos

# Modelo de Zorra
class Zorra(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.Integer, nullable=False)
    posicion = db.Column(db.Integer, nullable=False)
    estado = db.Column(db.String(20), nullable=False)  # puede ser 'acercamiento', 'empuje', 'coccion', 'cocinada', etc.
    fecha_acercamiento = db.Column(db.String(14))
    fecha_empuje = db.Column(db.String(14))
    fecha_coccion = db.Column(db.String(14))
    fecha_cocinada = db.Column(db.String(14))
    def __repr__(self):
        return f"<Zorra {self.nombre}, Posición {self.posicion}, Estado {self.estado},\n>"


# Modelo para almacenar eventos o logs de los cambios de estado de las zorras
class Evento(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tipo = db.Column(db.String(50), nullable=False)  # 'acercamiento', 'empuje', 'finalizacion', etc.
    zorra_id = db.Column(db.Integer, db.ForeignKey('zorra.id'), nullable=False)
    fecha_hora = db.Column(db.String(14), nullable=False)  # Fecha y hora del evento en formato '20240830052020'
    zorra = db.relationship('Zorra', backref=db.backref('eventos', lazy=True))

#Codigo para borrar datos de Zorra
@app.route('/borrarDatos', methods=['POST'])
def borrar_datos():
    # Get the database session
    db.session.query(Zorra).delete()
    db.session.commit()
    return 'Datos borrados con éxito!'

#################################################################################################################################
# Inicialización de la lista de posiciones de zorras
posiciones_zorras = [None] * 26  # Una lista de 26 posiciones
#################################################################################################################################
# Ruta principal
@app.route('/')
def index():
    return send_from_directory('static', 'index.html')

#################################################################################################################################
# Definir el umbral de tiempo en horas (por ejemplo, 12 horas)
UMBRAL_HORAS = 12

@app.route('/iniciar_horno', methods=['POST'])
def iniciar_horno():
    # Obtener la zorra en la posición 0, ya que es la más reciente
    zorra_actual = Zorra.query.filter_by(posicion=0).first()

    if zorra_actual:
        # Utilizar fecha de empuje si está disponible, si no, usar la de acercamiento
        fecha_ultimo_movimiento = None
        if zorra_actual.fecha_empuje:
            fecha_ultimo_movimiento = zorra_actual.fecha_empuje
        elif zorra_actual.fecha_acercamiento:
            fecha_ultimo_movimiento = zorra_actual.fecha_acercamiento

        # Si hay una fecha válida, procesarla
        if fecha_ultimo_movimiento:
            tiempo_ultimo_movimiento = datetime.strptime(fecha_ultimo_movimiento, "%Y%m%d%H%M%S")
            tiempo_actual = datetime.now()
            diferencia_tiempo = tiempo_actual - tiempo_ultimo_movimiento

            if diferencia_tiempo < timedelta(hours=UMBRAL_HORAS):
                # Si está dentro del umbral de tiempo, continuar el ciclo normal
                posiciones_zorras = [{"nombre": z.nombre, "posicion": z.posicion, "estado": z.estado} for z in Zorra.query.all()]
                return jsonify({"mensaje": "Continuar el ciclo", "posiciones_zorras": posiciones_zorras})
            else:
                # Si pasó el umbral de tiempo, reiniciar el ciclo
                return reiniciar_horno()
        else:
            # Si no hay ninguna fecha disponible, reiniciar el ciclo
            return reiniciar_horno()

    else:
        # Si no hay zorras en proceso o la posición 0 está vacía, reiniciar el ciclo
        return reiniciar_horno()

def reiniciar_horno():
    # Asignar la misma fecha de empuje para todas las zorras al reiniciar
    fecha_actual = datetime.now().strftime("%Y%m%d%H%M%S")

    # Reiniciar el ciclo con 6 zorras en posiciones 0-5
    posiciones_zorras = []
    for i in range(6):
        nueva_zorra = Zorra(
            nombre=i+1,
            posicion=5-i,
            estado="precarga",
            fecha_acercamiento=fecha_actual,  # Asignamos la misma fecha de empuje a todas
            fecha_empuje=fecha_actual  # También asignamos la fecha de empuje
        )
        db.session.add(nueva_zorra)
        db.session.commit()
        posiciones_zorras.append({
            "nombre": i+1,
            "posicion": 5-i,
            "estado": "precarga"
        })
    
    return jsonify({"mensaje": "Reinicio del horno con nuevas zorras", "posiciones_zorras": posiciones_zorras})

""" 
    @app.route('/iniciar_horno', methods=['POST'])
    def iniciar_horno():
        # Obtener la fecha actual y el umbral de 4 días
        fecha_actual = datetime.now()
        umbral_tiempo = fecha_actual - timedelta(days=4)
        
        # Verificar si hay zorras en la BD
        zorras_almacenadas = Zorra.query.order_by(Zorra.fecha_empuje.desc()).all()

        # Si no hay zorras almacenadas o hay menos de 6, crear las zorras numeradas del 1 al 6 en las posiciones 0-5
        if not zorras_almacenadas or len(zorras_almacenadas) < 6:
            for i in range(6):  # Inicializar 6 zorras en las posiciones 0-5
                nueva_zorra = Zorra(nombre=f"Zorra_{i+1}", posicion=i, estado="acercamiento", fecha_empuje=fecha_actual)
                db.session.add(nueva_zorra)
                posiciones_zorras[i] = nueva_zorra  # Añadir zorra a la lista de posiciones
            db.session.commit()
            return jsonify({
                "mensaje": "Horno iniciado con nuevas zorras",
                "zorras": [f"Zorra_{i+1}" for i in range(6)],
                "posiciones_zorras": [zorra.nombre for zorra in posiciones_zorras]
            })
        
        # Si ya hay más de 6 zorras almacenadas
        if len(zorras_almacenadas) > 6:
            # Filtrar zorras más recientes (últimos 22 registros)
            zorras_recientes = zorras_almacenadas[:22]
            
            # Verificar si la zorra más reciente tiene más de 4 días desde el empuje
            if zorras_recientes[0].fecha_empuje < umbral_tiempo:
                # Cargar solo las últimas 6 zorras si la fecha de empuje es mayor a 4 días
                zorras_a_cargar = zorras_almacenadas[:6]
            else:
                # Cargar las últimas 22 zorras si la fecha de empuje no supera el umbral
                zorras_a_cargar = zorras_recientes
            
            # Asignar las zorras a la lista posiciones_zorras
            for idx, zorra in enumerate(zorras_a_cargar):
                posiciones_zorras[idx] = zorra

            # Retornar las posiciones actualizadas al frontend
            return jsonify({
                "mensaje": "Horno iniciado con zorras almacenadas",
                "zorras": [zorra.nombre for zorra in zorras_a_cargar],
                "posiciones_zorras": [zorra.nombre for zorra in posiciones_zorras]
            })

        return jsonify({"mensaje": "Error en la carga de zorras"})

    # Ruta para actualizar la lista de posiciones desde el frontend
    @app.route('/actualizar_posiciones', methods=['POST'])
    def actualizar_posiciones():
        data = request.get_json()
        
        # Verificamos que la lista recibida tenga el tamaño correcto
        if 'posiciones' in data and len(data['posiciones']) == 26:
            global posiciones_zorras
            posiciones_zorras = data['posiciones']
            return jsonify({"mensaje": "Posiciones actualizadas correctamente"}), 200
        else:
            return jsonify({"error": "Datos incorrectos"}), 400 
            
"""

#################################################################################################################################
@app.route('/proceso_horno', methods=['POST'])
def proceso_horno():
    # Recibimos la data del POST (ya sea "acercamiento" o "empuje")
    data = request.get_json()

    # Obtenemos las zorras actuales en el sistema ordenadas por su posición
    posiciones_zorras = Zorra.query.order_by(Zorra.posicion).all()
    total_zorras = len(posiciones_zorras)

    # Lógica para acercar una zorra
    if data['accion'] == 'acercamiento':
        if total_zorras < 24:
            # Obtener el nombre de la última zorra y extraer el número
            if posiciones_zorras:
                ultimo_numero = int(posiciones_zorras[0].nombre)
            else:
                ultimo_numero = 0  # Si no hay zorras, empezamos con 0

            # Generar el nuevo nombre para la zorra
            nuevo_nombre = ultimo_numero + 1

            nueva_zorra = Zorra(
                nombre=nuevo_nombre,
                posicion=24,  # Asignamos la posición 24, que es la zona de acercamiento
                estado='acercamiento',
                fecha_acercamiento=time.strftime("%Y%m%d%H%M%S")
            )
            posiciones_zorras.insert(0, nueva_zorra)  # Insertamos la nueva zorra en la posición 0 (acercamiento)
            db.session.add(nueva_zorra)
            db.session.commit()

            # Imprimir con una representación clara de las zorras
            print("posicines zorras luego de acercar con menos de 24 zorras:", [repr(z) for z in posiciones_zorras])

            return jsonify({"mensaje": f"Zorra {nuevo_nombre} creada y acercada a la posición 24"})
        else:
            # Si hay 24 zorras, se reutiliza la última zorra
            ultima_zorra = posiciones_zorras[-1]
            ultima_zorra.estado = 'acercamiento'
            ultima_zorra.fecha_acercamiento = time.strftime("%Y%m%d%H%M%S")

            # Borrar datos antiguos de la zorra (empuje, cocción, cocinada)
            ultima_zorra.fecha_empuje = None
            ultima_zorra.fecha_coccion = None
            ultima_zorra.fecha_cocinada = None

            db.session.commit()
            print("posicines zorras luego de acercar con 24 zorras:", [repr(z) for z in posiciones_zorras])

            return jsonify({"mensaje": f"Zorra {ultima_zorra.nombre} reutilizada y acercada a la posición 24"})

    # Lógica para empujar una zorra
    elif data['accion'] == 'empuje':
        # Buscamos la zorra en la posición 24 (última zorra en acercamiento)
        zorra_acercada = posiciones_zorras[0] if posiciones_zorras else None
        if zorra_acercada and zorra_acercada.estado == 'acercamiento':
            zorra_acercada.estado = 'empuje'
            zorra_acercada.fecha_empuje = time.strftime("%Y%m%d%H%M%S")
            db.session.commit()

            # Después de 30 minutos, simular el movimiento de todas las zorras
            time.sleep(1800)  # Simulación de espera de 30 minutos

            # Movemos todas las zorras una posición hacia adelante
            posiciones_zorras.append(posiciones_zorras.pop(0))

            # Actualizamos la base de datos con las nuevas posiciones
            for i, zorra in enumerate(posiciones_zorras):
                zorra.posicion = i
            db.session.commit()

            return jsonify({"mensaje": f"Zorra {zorra_acercada.nombre} empujada y lista para cocción"})
    
    return jsonify({"error": "Acción no válida o faltan datos"}), 400


#################################################################################################################################
# Lógica para manejar el ciclo de la lista de posiciones
@app.route('/mover_zorras', methods=['POST'])
def mover_zorras():
    # Este endpoint simula el movimiento cíclico de las zorras en el horno
    # La zorra en la posición 25 (empuje) pasa a la posición 0 (inicio del ciclo)

    data = request.get_json()
    accion = data['accion']

    if accion == 'correr_ciclo':
        # Mover todas las zorras una posición hacia adelante
        zorra_a_posicion_0 = posiciones_zorras[25]
        for i in range(25, 0, -1):
            posiciones_zorras[i] = posiciones_zorras[i-1]
        posiciones_zorras[0] = zorra_a_posicion_0
        
        # Actualizar las posiciones en la base de datos
        for i, zorra in enumerate(posiciones_zorras):
            if zorra:
                zorra.posicion = i
                db.session.commit()
        
        return jsonify({"mensaje": "Zorras movidas", "posiciones": [zorra.nombre if zorra else None for zorra in posiciones_zorras]})
    
    return jsonify({"error": "Acción no válida"}), 400

#################################################################################################################################
# Lógica para manejar la llegada de la zorra a la posición 9 (cocinada)
@app.route('/procesar_coccion', methods=['POST'])
def procesar_coccion():
    # Verificar si alguna zorra está en la posición 9 y cambiar su estado a 'cocinada'
    zorra_en_posicion_9 = posiciones_zorras[9]

    if zorra_en_posicion_9 and zorra_en_posicion_9.estado != 'cocinada':
        zorra_en_posicion_9.estado = 'cocinada'
        zorra_en_posicion_9.fecha_cocinada = time.strftime("%Y%m%d%H%M%S")
        db.session.commit()

        # Guardar la zorra cocinada en el histórico
        nueva_zorra_cocinada = Zorra(
            nombre=zorra_en_posicion_9.nombre,
            posicion=zorra_en_posicion_9.posicion,
            estado='cocinada',
            fecha_coccion=zorra_en_posicion_9.fecha_coccion,
            fecha_cocinada=zorra_en_posicion_9.fecha_cocinada
        )
        db.session.add(nueva_zorra_cocinada)
        db.session.commit()

        return jsonify({"mensaje": f"Zorra {zorra_en_posicion_9.nombre} cocinada y guardada en el histórico"})
    
    return jsonify({"error": "No hay zorra en posición 9 o ya está cocinada"}), 400

#################################################################################################################################

#################################################################################################################################
# Lanzamiento de la app
if __name__ == '__main__':
    app.run(debug=True)